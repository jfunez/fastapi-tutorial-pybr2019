from .schemas import Company, Ticket, Comment
from .main import database


def create_tables():
    with database:
        database.create_tables([Company, Ticket, Comment])


if __name__ == "__main__":
    create_tables()
