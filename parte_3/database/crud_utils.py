from .schemas import Company, BaseModel, Ticket
from models.ticket import TicketCreateModel


def company_create(name: str):
    return Company.create(name=name)


def company_list():
    print(list(Company.select()))
    return Company.select()


def company_get_by_name(name):
    return Company.get(name=name)


def company_get_by_id(id):
    return Company.get(id=id)


def company_update(id, name):
    Company.update(name=name).where(Company.id == id).execute()
    return Company.get(id=id)


def company_delete_by_name(name):
    return Company.get(name=name).delete()


def company_delete_by_id(id):
    return Company.get(id=id).delete()


def tickets_in_company(company_id: int):
    q = Ticket.select().where(Ticket.company == company_id).execute()
    return q


def ticket_create(company_id: int, ticket: TicketCreateModel):
    print(ticket.dict())
    result = Ticket.create(**ticket.dict(), company=company_id)
    print(result)
    return result


def ticket_get(id):
    return Ticket.get(id=id)
