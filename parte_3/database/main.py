from peewee import SqliteDatabase, Model
import os


database = SqliteDatabase("suportinho.db")


class BaseModel(Model):
    class Meta:
        database = database

