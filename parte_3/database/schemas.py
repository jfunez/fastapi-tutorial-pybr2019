from .main import BaseModel
from peewee import (
    CharField,
    IntegerField,
    TextField,
    DateTimeField,
    DateField,
    BooleanField,
    ForeignKeyField,
)
import datetime


class Company(BaseModel):
    name = TextField(unique=True)


class Ticket(BaseModel):

    STATUS_CHOICES = (
        ("O", "Open"),
        ("F", "Finished"),
        ("C", "Cancelled"),
        ("W", "Waiting information"),
    )
    name = CharField()
    status = CharField(max_length=1, choices=STATUS_CHOICES)
    timestamp = DateTimeField(default=datetime.datetime.now)
    description = TextField()
    company = ForeignKeyField(Company, backref="tickets")


class Comment(BaseModel):
    text = TextField()
