from fastapi import APIRouter, HTTPException
from peewee import IntegrityError
from database.crud_utils import (
    company_list,
    company_create,
    company_delete_by_id,
    company_get_by_id,
    company_update,
)
from models.company import CompanyModel
from typing import List
from .ticket import router as ticket_router

router = APIRouter()


@router.get("/", tags=["company"], response_model=List[CompanyModel])
def list_companies():
    return list(company_list())


@router.post("/", tags=["company"], response_model=CompanyModel)
def create_company(name: str):
    try:
        return company_create(name=name)
    except IntegrityError:
        raise HTTPException(
            status_code=500, detail=f"Company {name} already exists."
        )


@router.get("/{company_id}", tags=["company"], response_model=CompanyModel)
def get_company(company_id: int):
    return company_get_by_id(company_id)


@router.put("/{company_id}", tags=["company"], response_model=CompanyModel)
def update_company(company_id: int, name: str):
    return company_update(company_id, name)


@router.delete("/{company_id}", tags=["company"])
def delete_company(company_id: int):
    try:
        company_delete_by_id(company_id)
        return "ok"
    except:
        raise HTTPException(status_code=404, detail="Item not found")


router.include_router(
    ticket_router, prefix="/{company_id}/ticket", tags=["ticket"]
)

