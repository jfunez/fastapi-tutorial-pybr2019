from fastapi import APIRouter, HTTPException
from typing import List
from models.ticket import TicketModel, TicketCreateModel
from database.crud_utils import tickets_in_company, ticket_get, ticket_create


router = APIRouter()


@router.get("/", response_model=List[TicketModel])
def list_tickets(company_id: int):
    return list(tickets_in_company(company_id))


@router.post("/", response_model=TicketModel, status_code=201)
def create_tickets(company_id: int, ticket: TicketCreateModel):
    return ticket_create(company_id, ticket)
