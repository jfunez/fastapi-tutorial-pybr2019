from fastapi import FastAPI, Depends
from endpoints import company
from peewee import SqliteDatabase
import os


app = FastAPI()



@app.get("/")
async def read_main():
    return {"msg": "Hello World"}


app.include_router(
    company.router, prefix="/company"
)
