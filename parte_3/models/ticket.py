from .utils import BaseModel
from enum import Enum
from pydantic import BaseModel as PydanticBase
from pydantic import ValidationError, validator

class StatusChoices(Enum):
    open = "O"
    finished = "F"
    cancelled = "C"
    waiting_information = "W"


class TicketModel(BaseModel):
    id: int
    name: str
    status: StatusChoices
    description: str
    company_id: int


class TicketCreateModel(BaseModel):
    name: str
    status: StatusChoices = StatusChoices.open.value
    description: str
