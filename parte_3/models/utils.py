from pydantic import BaseModel as pydantic_BaseModel


class BaseModel(pydantic_BaseModel):
    class Config:
        orm_mode = True

