from .utils import BaseModel


class CommentModel(BaseModel):
    comment_id: int
    text: str
