from .utils import BaseModel


class CompanyModel(BaseModel):
    id: int
    name: str
