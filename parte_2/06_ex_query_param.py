from fastapi import FastAPI

app = FastAPI()


@app.get("/todo/")
def list_tasks(completed: bool = False):
    # usamos o argumento completed para filtrar
    return {"completed": completed}
