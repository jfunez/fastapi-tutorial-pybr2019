from fastapi import FastAPI

app = FastAPI()


@app.get("/todo/delete/{item_id}")
def delete_item(item_id):
    return {"item_id": item_id}
