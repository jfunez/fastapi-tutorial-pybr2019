from fastapi import FastAPI, HTTPException
from typing import List
from pydantic import BaseModel

app = FastAPI()


class Task(BaseModel):
    id: int
    description: str = None
    is_done: bool = False


class TaskIn(BaseModel):
    description: str = None
    is_done: bool = False


lista_de_tarefas = [
    {
        "id": 1,
        "description": "primeira tarefa",
        "is_done": False
    },
]


@app.get('/')
def root():
    return {"message": "sucesso"}


@app.get('/todo/', response_model=List[Task])
def list_all_tasks():
    """
    retorna a lista com todas as tarefas
    """
    return lista_de_tarefas


@app.post('/todo/', response_model=List[Task])
def add_task(task_decription: str):
    """
    add nova task
    """
    new_task = {
        "id": len(lista_de_tarefas) + 1,
        "description": task_decription,
        "is_done": False
    }
    lista_de_tarefas.append(new_task.dict())
    return lista_de_tarefas


@app.delete('/todo/{task_id}')
def remove_task(task_id: int):
    for position, task in enumerate(lista_de_tarefas):
        if task['id'] == task_id:
            lista_de_tarefas.pop(position)

    return {"success": True}


@app.put('/todo/update/{task_id}', response_model=Task)
def update_task(task_id: int):
    target_task = None
    for task in lista_de_tarefas:
        if task["id"] == task_id:
            target_task = task

    if target_task:
        target_task["is_done"] = True
    else:
        raise HTTPException(status_code=404, detail="TASK NÃO ENCOTRADA")
    return target_task
