def root(nome: string, sobrenome: string, idade: int, is_humano: bool):
    # o código da função aqui
    nome_completo = f"{nome} {sobrenome}"
    if is_humano:
        return f"o humano: {nome_completo} tem {idade} anos de idade"
    else:
        return f"o ET: {nome_completo} tem {idade} anos de idade"
